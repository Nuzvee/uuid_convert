# Description: Converts the UUID from clipboard and saves the converted UUID to clipboard.
# Author: J.A AKA Nuzvee 2019

import sys, os
import win32clipboard ### Install with pip install pywin32

def convert_UUID(UUID):

# removes any dashes
    UUID = UUID.replace("-","")
   
# Rearranges the UUID order    
    uuidpt_1 = UUID[0:2:1]
    uuidpt_2 = UUID[2:4:1]
    uuidpt_3 = UUID[4:6:1]
    uuidpt_4 = UUID[6:8:1]
    uuidpt_5 = UUID[8:10:1]
    uuidpt_6 = UUID[10:12:1]
    uuidpt_7 = UUID[12:14:1]
    uuidpt_8 = UUID[14:16:1]
    uuidpt_9 = UUID[16:18:1]
    uuidpt_10 = UUID[18:20:1]
    uuidpt_11 = UUID[20:32:1]
    
# Sets string with rearranged order    
    Conv_UUID = uuidpt_4+uuidpt_3+uuidpt_2+uuidpt_1+"-"+\
                uuidpt_6+uuidpt_5+"-"+\
                uuidpt_8+uuidpt_7+"-"+\
                uuidpt_9+uuidpt_10+"-"+\
                uuidpt_11

    return Conv_UUID


if __name__ == '__main__':
# Gets string from clipboard    
    win32clipboard.OpenClipboard()
    data = win32clipboard.GetClipboardData()
    win32clipboard.CloseClipboard()

# Converts string with convert_UUID    
    UUID = convert_UUID(data)
    
# Saves converted UUID back to clipboard
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(UUID)
    win32clipboard.CloseClipboard()
